# VSSH

VSSH is a very simple CLI utiliy that parse a given SecureCRT session and return a SSH command formatted according to a template.

The simplest way to use it is to just run it against a SecureCRT session:
```
$ cat path/to/your/session.ini
S:"Username"=root
D:"[SSH2] Port"=00000016
S:"Hostname"=127.0.0.1
S:"Identity Filename V2"=
[...]
$ vssh -path path/to/your/session.ini
ssh root@127.0.0.1
```

It is possible to customize the template to use vssh with other tools, like sshpass and scp
```
$ vssh -path path/to/your/session.ini -template "sshpass -e ssh {{.User}}@{{.Host}}"
sshpass -e ssh root@127.0.0.1
```
vssh use standard [golang templates](https://golang.org/pkg/text/template/).  
Templates have access to the `Session` struct.

Note: for now, only the `"Username"`, `"[SSH2] Port"`, `"Hostname"` and `"Identity Filename V2"` directive in a SecureCRT session are parsed and used.  
Also, SecureCRT store the password encrypted inside the session, so use SSH Keys when possible.  

Why? At work SecureCRT is the standard to exchange sessions to servers, and I'm not really a fan of it.