package vssh

import (
	"bufio"
	"reflect"
	"strings"
	"testing"
)

func TestParseSecureCRTSession(t *testing.T) {
	validSession := `
	S:"Username"=root
	D:"[SSH2] Port"=00000017
	S:"Hostname"=127.0.0.1
	S:"Identity Filename V2"=${VDS_CONFIG_PATH}\key.pem
	`
	emptyLines := `
	S:"Username"=
	D:"[SSH2] Port"=
	S:"Hostname"=
	S:"Identity Filename V2"=
	`
	type args struct {
		s *bufio.Scanner
	}
	tests := []struct {
		name string
		args args
		want Session
	}{
		{
			name: "Empty file returns an empty session",
			args: args{s: bufio.NewScanner(strings.NewReader(``))},
			want: Session{},
		},
		{
			name: "Invalid line returns an empty session",
			args: args{s: bufio.NewScanner(strings.NewReader(`<>!#@$AAS:2234=11asd`))},
			want: Session{},
		},
		{
			name: "Valid session should return a valid session",
			args: args{s: bufio.NewScanner(strings.NewReader(validSession))},
			want: Session{User: "root", Port: 23, Host: "127.0.0.1", IdentityFile: "key.pem"},
		},
		{
			name: "Empty lines should return a valid empty session",
			args: args{s: bufio.NewScanner(strings.NewReader(emptyLines))},
			want: Session{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseSecureCRTSession(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseSecureCRTSession() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_portStringtoInt(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Should return 22",
			args: args{s: "00000016"},
			want: 22,
		},
		{
			name: "Empty lines should return 22",
			args: args{s: ""},
			want: 22,
		},
		{
			name: "Invalid numbers should return 22",
			args: args{s: "this is not a number"},
			want: 22,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := portStringtoInt(tt.args.s); got != tt.want {
				t.Errorf("portStringtoInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultTemplate(t *testing.T) {
	type args struct {
		s Session
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Complete session sample",
			args: args{s: Session{User: "root", Port: 23, Host: "127.0.0.1", IdentityFile: "key.pem"}},
			want: "ssh {{.User}}@{{.Host}} -p {{.Port}} -i {{.IdentityFile}}",
		},
		{
			name: "Sessions with port 22 should not specify the port in the template",
			args: args{s: Session{User: "root", Port: 22, Host: "127.0.0.1", IdentityFile: "key.pem"}},
			want: "ssh {{.User}}@{{.Host}} -i {{.IdentityFile}}",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultTemplate(tt.args.s); got != tt.want {
				t.Errorf("DefaultTemplate() = %v, want %v", got, tt.want)
			}
		})
	}
}
