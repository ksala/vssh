package vssh

import (
	"bufio"
	"fmt"
	"log"
	"path"
	"strconv"
	"strings"
)

// Session represent a SecureCRT/SSH session
type Session struct {
	User         string
	Host         string
	IdentityFile string
	Port         int
}

// ParseSecureCRTSession parse a SecureCRT .ini session into a Session struct
func ParseSecureCRTSession(s *bufio.Scanner) Session {
	session := Session{}
	for s.Scan() {
		line := s.Text()

		key, value, err := parseLine(line)
		if err != nil {
			continue
		}

		switch key {
		case "Username":
			session.User = value
		case "Hostname":
			session.Host = value
		case "[SSH2] Port":
			session.Port = portStringtoInt(value)
		case "Identity Filename V2":
			session.IdentityFile = path.Base(strings.Replace(value, `\`, `/`, -1))
		}

	}
	return session
}

func parseLine(s string) (string, string, error) {
	// Skip on empty lines
	// Skip if the line begin with a space, because it's
	// not in the tag:key=value format we expect
	if len(s) <= 0 || s[0] == ' ' {
		return "", "", fmt.Errorf("LineNotValid")
	}

	l := strings.FieldsFunc(s, func(c rune) bool {
		switch c {
		case ':':
			return true
		case '=':
			return true
		default:
			return false
		}
	})

	if len(l) < 3 {
		return "", "", fmt.Errorf("LineNotValid")
	}

	key := strings.Trim(l[1], `"`)
	value := strings.Join(l[2:], "")
	return key, value, nil
}

// portStringtoInt takes a string in SecureCRT port format
// for example '00000016'
// And return the right port number
// The SecureCRT port format is just a base16 number padded to 8 digits
func portStringtoInt(s string) int {
	port, err := strconv.ParseInt(s, 16, 0)
	if err != nil {
		log.Printf("Could not parse port, defaulting to 22. Err: %s\n", err)
		return 22
	}
	return int(port)
}

// DefaultTemplate guess the best template for a given session
func DefaultTemplate(s Session) string {
	template := "ssh {{.User}}@{{.Host}}"
	if s.Port != 22 {
		template += " -p {{.Port}}"
	}
	if s.IdentityFile != "" {
		template += " -i {{.IdentityFile}}"
	}

	return template
}
