package main

import (
	"bufio"
	"flag"
	"fmt"
	"html/template"
	"log"
	"os"

	"gitlab.com/ksala/vssh"
)

func main() {
	t := flag.String("template", "", "Template")
	path := flag.String("path", "", "Path of the SecureCRT session to parse")
	flag.Parse()
	if *path == "" {
		log.Fatal("-path is a required argument!")
	}

	ini, err := os.Open(*path)
	if err != nil {
		log.Fatalf("Could not open %s, err: %s", *path, err)
	}
	defer ini.Close()
	scanner := bufio.NewScanner(ini)
	session := vssh.ParseSecureCRTSession(scanner)
	tmpl := template.New("")
	if *t == "" {
		tmpl = template.Must(tmpl.Parse(vssh.DefaultTemplate(session)))
	} else {
		tmpl = template.Must(tmpl.Parse(*t))
	}

	if err = tmpl.Execute(os.Stdout, session); err != nil {
		log.Fatalf("Couldn't execute template with session %+v, err: %s", session, err)
	}
	fmt.Println()
}
